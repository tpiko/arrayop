# pylint: disable=line-too-long
# pylint: disable=invalid-name

from typing import Tuple
from typing import List
from typing import Union
import array
import math as math

class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type = None, data: Union[Tuple, List] = None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        _check_shape(shape)
        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n_elements = 1
        for s in self.__shape:
            n_elements *= s

        if data is None:
            if dtype == int:
                self.__data = array.array('i', [0]*n_elements)
            elif dtype == float:
                self.__data = array.array('f', [0]*n_elements)
        else:
            if len(data) != n_elements:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 1 or ind[ax] > self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds [1, {:}]'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not isinstance(indice, tuple):
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data

    def _to_string(self):
        xD = len(self.shape)
        max = 1
        for i in self.__data:
            if len(str(i)) > max:
                max = len(str(i))
        if xD == 1:
            return print1D(self, max)
        elif xD == 2:
            return print2D(self, max)
        elif xD == 3:
            return print3D(self, max)
        else:
            raise Exception('Ni implementiranega izpisa.')


    def _find(self, src):
        length = len(self.shape)
        a1 = self.shape
        a2 = []

        prod = 1
        for i in a1:
            prod *= i
        for i in range(len(a1)):
            if i == 0:
                a2.append(prod / a1[i])
            else:
                a2.append(a2[i-1] / a1[i])
        r = []
        idx = 0
        for i in self.__data:
            if i == src:
                for j in range(length):
                    r.append((math.floor(idx / a2[j])) % a1[j])
            idx += 1
        if len(r) == 0:
            return "V matriki ni tega elementa."
        else:
            izpis = "("
            for k in range(0, int(len(r)), len(self.shape)):
                izpis += "("
                for j in range(len(self.shape)):
                    if j != len(self.shape) - 1:
                        izpis += str(r[k+j]) + ","
                    else:
                        izpis += str(r[k+j])
                izpis += ")"
            izpis += ")"
            return izpis


    """
    1D
    tmp = list(newarr.__data);
    newarr.__data = tuple(tmp)
    
    2D
    tmp = list(newarr.__data[i * vrsticaSize: ((i + 1) * vrsticaSize)])
    newarr.__data[i * vrsticaSize: ((i + 1) * vrsticaSize)] = tmp
    """

    def _merge_sort_improved(self, stype="vrs"):
        newarr = self
        if stype not in ["vrs", "stp"]:
            raise Exception('Type must be \'vrs\' or \'stp\'')
        length = len(newarr.shape)
        if length == 1:  # Preveri ce je 1D
            mergeSort(newarr.__data)
            return newarr
        elif length == 2:  # Preveri ce je 2D
            if stype == "vrs":
                for i in range(newarr.shape[0]):  # Vse vrstice da v array
                    vrsticaSize = newarr.shape[1]
                    tmp = list(newarr.__data[i * vrsticaSize: ((i + 1) * vrsticaSize)])
                    mergeSort(tmp)  # Merge sort arraya
                    newarr.__data[i * vrsticaSize: ((i + 1) * vrsticaSize)] = tmp
                return newarr
            elif stype == "stp":
                for i in range(newarr.shape[1]):
                    stolpecSize = newarr.shape[0]
                    tmp = list(newarr.__data[i::stolpecSize])
                    mergeSort(tmp)
                    newarr.__data[i::stolpecSize] = tmp
                return newarr
        else:
            raise Exception('No method for sorting 3D matrices.')


    def _merge_sort(self, stype="vrs"):
        newarr = self
        if stype not in ["vrs", "stp"]:
            raise Exception('Type must be \'vrs\' or \'stp\'')
        length = len(newarr.shape)
        if length == 1: #Preveri ce je 1D
            mergeSort(newarr.__data)
            return newarr
        elif length == 2: #Preveri ce je 2D
            if stype == "vrs":
                for i in range(newarr.shape[0]): #Vse vrstice da v array
                    tmp = vrsticaToArray(newarr, i) #V array
                    mergeSort(tmp) #Merge sort arraya
                    for j in range(len(tmp)): #Sortiran array zdruzi nazaj v vrstico
                        newarr[i, j] = tmp[j] #new array je sortiran array.
                return newarr
            elif stype == "stp":
                for i in range(newarr.shape[1]):
                    tmp = stolpecToarray(newarr, i)
                    mergeSort(tmp)
                    for j in range(len(tmp)):
                        newarr[j, i] = tmp[j]
                return newarr
        else:
            raise Exception('No method for sorting 3D matrices.')


    def __add__(self, other):
        if type(other) is BaseArray:
            if self.dtype is int and other.dtype is int:
                newarr = BaseArray(self.shape, int)
            else:
                newarr = BaseArray(self.shape, float)
            for i in range(0, len(self.__data), +1):
                newarr.__data[i] = self.__data[i] + other.__data[i]
            return newarr
        else:
            raise Exception('Type of other is not valid.')


    def __sub__(self, other):
        if type(other) is BaseArray:
            if self.dtype is int and other.dtype is int:
                newarr = BaseArray(self.shape, int)
            else:
                newarr = BaseArray(self.shape, float)
            for i in range(0, len(self.__data), +1):
                newarr.__data[i] = self.__data[i] - other.__data[i]
            return newarr
        else:
            raise Exception('Type of other is not valid.')


    def __mul__(self, other):
        if type(other) is BaseArray:
            if self.shape[1] != other.shape[0]:
                raise Exception(f'2D multiplication error invalid shapes {self.shape:} {other.shape:}')
            if len(self.shape) > 2 or len(other.shape) > 2:
                raise Exception('3D multiplication error.')

            if self.dtype is int and other.dtype is int:
                newarr = BaseArray((self.shape[0], other.shape[1]), int)
            else:
                newarr = BaseArray((self.shape[0], other.shape[1]), float)

            for i in range(newarr.shape[0]):
                for j in range(newarr.shape[1]):
                    for k in range(self.shape[1]):
                        newarr[i+1, j+1] += self[i+1, k+1] * other[k+1, j+1]
            return newarr
        elif type(other) in [int, float]:
            newarr = BaseArray(self.shape, type(other))
            for i in range(0, len(self.__data), +1):
                newarr.__data[i] = self.__data[i] * other
            return newarr
        else:
            raise Exception('Type of other is not valid.')


    def __truediv__(self, other):
        if type(other) in [int, float]:
            newarr = BaseArray(self.shape, float)
            for i in range(0, len(self.__data), +1):
                newarr.__data[i] = self.__data[i] / other
            return newarr
        else:
            raise Exception('Type of other is not valid.')


    def __pow__(self, power, modulo=None):
        newarr = self
        for i in range(power-1):
            newarr = self.__mul__(newarr)
        return newarr


    def __log__(self, base):
        if type(base) in [int, float]:
            newarr = BaseArray(self.shape, float)
            for i in range(0, len(self.__data), +1):
                newarr.__data[i] = math.log(self.__data[i], base)
            return newarr
        else:
            raise Exception('Type of base is not valid.')


def _check_shape(shape):
    if not isinstance(shape, (tuple, list)):
        raise Exception(f'shape {shape:} is not a tuple or list')
    for dim in shape:
        if not isinstance(dim, int):
            raise Exception(f'shape {shape:} contains a non integer {dim:}')


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if isinstance(inds, slice):
        start, stop, step = inds.start, inds.stop, inds.step
        if start is None:
            start = 1
        if step is None:
            step = 1
        if stop is not None:
            stop += 1
        inds_itt = range(0, s+1)[start:stop:step]
    else:  # type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += (inds[n]-1)*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if not indice:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True


def print1D(arr: BaseArray, max: int):
    izpis = ""
    for i in arr:
        izpis = izpis + str(i) + " "
    return izpis


def print2D(arr: BaseArray, max: int):
    izpis = ""
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            num = str(arr[i+1, j+1])
            for n in range(max - len(str(num))):
                izpis += " "
            izpis = izpis + str(num) + " "
        izpis += "\n"
    return izpis


def print3D(arr: BaseArray, max: int):
    izpis = ""
    for k in range(arr.shape[0]):
        for i in range(arr.shape[1]):
            for j in range(arr.shape[2]):
                num = str(arr[k+1, i+1, j+1])
                for n in range(max - len(str(num))):
                    izpis += " "
                izpis = izpis + str(num) + " "
            izpis += "\n"
        izpis += "\n"
    return izpis


def mergeSort(arr):
    if len(arr) > 1:
        mid = len(arr) // 2
        L = arr[:mid]
        R = arr[mid:]

        mergeSort(L)
        mergeSort(R)

        i = j = k = 0

        while i < len(L) and j < len(R):
            if L[i] < R[j]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1

        while i < len(L):
            arr[k] = L[i]
            i += 1
            k += 1

        while j < len(R):
            arr[k] = R[j]
            j += 1
            k += 1


def vrsticaToArray(arr: BaseArray, k: int):
    tmp = []
    stolpci = arr.shape[1]
    for i in range(stolpci):
        tmp.append(arr[k, i])
    return tmp


def stolpecToarray(arr: BaseArray, k: int):
    tmp = []
    vrstice = arr.shape[0]
    for i in range(vrstice):
        tmp.append(arr[i, k])
    return tmp


def toArray(arr: BaseArray):
    tmp = []
    for i in arr:
        tmp.append(i)
    return tmp
