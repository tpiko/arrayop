from unittest import TestCase
from myarray.basearray import BaseArray
import myarray.basearray as ndarray


class TestArray(TestCase):
    def test___init__(self):

        # simple init tests, other things are tester later
        try:
            a = BaseArray((2,))
            a = BaseArray([2, 5])
            a = BaseArray((2, 5), dtype=int)
            a = BaseArray((2, 5), dtype=float)
        except Exception as e:
            self.fail(f'basic constructor test failed, with exception {e:}')

        # test invalid parameters as shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray(5)
        self.assertEqual(('shape 5 is not a tuple or list',),
                         cm.exception.args)

        # test invalid value in shape
        with self.assertRaises(Exception) as cm:
            a = BaseArray((4, 'a'))
        self.assertEqual(('shape (4, \'a\') contains a non integer a',),
                         cm.exception.args)

        # TODO test invalid dtype

        # test valid data param, list, tuple
        a = BaseArray((3, 4), dtype=int, data=list(range(12)))
        a = BaseArray((3, 4), dtype=int, data=tuple(range(12)))

        # test invalid data param
        # invalid data length
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 4), dtype=int, data=list(range(12)))
        self.assertEqual(('shape (2, 4) does not match provided data length 12',),
                         cm.exception.args)

        # inconsistent data type
        with self.assertRaises(Exception) as cm:
            a = BaseArray((2, 2), dtype=int, data=(4, 'a', (), 2.0))
        self.assertEqual(('provided data does not have a consistent type',),
                         cm.exception.args)

    def test_shape(self):
        a = BaseArray((5,))
        self.assertTupleEqual(a.shape, (5,))
        a = BaseArray((2, 5))
        self.assertTupleEqual(a.shape, (2, 5))
        a = BaseArray((3, 2, 5))
        self.assertEqual(a.shape, (3, 2, 5))

    def test_dtype(self):
        # test if setting a type returns the set type
        a = BaseArray((1,))
        self.assertIsInstance(a[1], float)
        a = BaseArray((1,), int)
        self.assertIsInstance(a[1], int)
        a = BaseArray((1,), float)
        self.assertIsInstance(a[1], float)

    def test_get_set_item(self):
        # test if setting a value returns the value
        a = BaseArray((1,))
        a[1] = 1
        self.assertEqual(1, a[1])
        # test for multiple dimensions
        a = BaseArray((2, 1))
        a[1, 1] = 1
        a[2, 1] = 1
        self.assertEqual(1, a[1, 1])
        self.assertEqual(1, a[2, 1])

        # test with invalid indices
        a = BaseArray((2, 2))
        with self.assertRaises(Exception) as cm:
            a[4, 1] = 0
        self.assertEqual(('indice (4, 1), axis 0 out of bounds [1, 2]',),
                         cm.exception.args)

        # test invalid type of ind
        with self.assertRaises(Exception) as cm:
            a[1, 1.1] = 0
        self.assertEqual(cm.exception.args,
                         ('(1, 1.1) is not a valid indice',))

        with self.assertRaises(Exception) as cm:
            a[1, 'a'] = 0
        self.assertEqual(('(1, \'a\') is not a valid indice',),
                         cm.exception.args)

        # test invalid number of ind
        with self.assertRaises(Exception) as cm:
            a[2] = 0
        self.assertEqual(('indice (2,) is not valid for this myarray',),
                         cm.exception.args)

        # test data intialized via data parameter
        a = BaseArray((2, 3, 4), dtype=int, data=tuple(range(24)))
        self.assertEqual(3, a[1, 1, 4])
        self.assertEqual(13, a[2, 1, 2])

        # TODO enter invalid type

    def test_iter(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in a]
        self.assertListEqual([1, 2, 3, 4, 5, 6], a_vals)

    def test_reversed(self):
        a = BaseArray((2, 3), dtype=int)
        a[1, 1] = 1
        a[1, 2] = 2
        a[1, 3] = 3
        a[2, 1] = 4
        a[2, 2] = 5
        a[2, 3] = 6

        a_vals = [v for v in reversed(a)]
        self.assertListEqual([6, 5, 4, 3, 2, 1], a_vals)

    def test_contains(self):
        a = BaseArray((1,), dtype=float)
        a[1] = 1

        self.assertIn(1., a)
        self.assertIn(1, a)
        self.assertNotIn(0, a)

    def test_shape_to_steps(self):
        shape = (4, 2, 3)
        steps_exp = (6, 3, 1)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = [1]
        steps_exp = (1,)
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

        shape = []
        steps_exp = ()
        steps_res = ndarray._shape_to_steps(shape)
        self.assertTupleEqual(steps_exp, steps_res)

    def test_multi_to_lin_ind(self):
        # shape = 4, 2,
        # 0 1
        # 2 3
        # 4 5
        # 6 7
        steps = 2, 1
        multi_lin_inds_pairs = (((1, 1), 0),
                                ((2, 2), 3),
                                ((4, 1), 6),
                                ((4, 2), 7))
        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_exp, lin_ind_res)

        # shape = 2, 3, 4
        #
        # 0 1 2 3
        # 4 5 6 7
        #
        #  8  9 10 11
        # 12 13 14 15
        steps = 8, 4, 1
        multi_lin_inds_pairs = (((1, 1, 1), 0),
                                ((2, 2, 1), 12),
                                ((1, 1, 4), 3),
                                ((2, 2, 3), 14))

        for multi_inds, lin_ind_exp in multi_lin_inds_pairs:
            lin_ind_res = ndarray._multi_to_lin_ind(multi_inds, steps)
            self.assertEqual(lin_ind_res, lin_ind_exp)

    def test_is_valid_instance(self):
        self.assertTrue(ndarray._is_valid_indice(2))
        self.assertTrue(ndarray._is_valid_indice((2, 1)))

        self.assertFalse(ndarray._is_valid_indice(()))
        self.assertFalse(ndarray._is_valid_indice([2]))
        self.assertFalse(ndarray._is_valid_indice(2.0))
        self.assertFalse(ndarray._is_valid_indice((2, 3.)))

    def test_multi_ind_iterator(self):
        shape = (30,)
        ind = (slice(10, 20, 3),)
        ind_list_exp = ((10,),
                        (13,),
                        (16,),
                        (19,))
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        shape = (5, 5, 5)
        ind = (2, 2, 2)
        ind_list_exp = ((2, 2, 2),)
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (2, slice(3), 2)
        ind_list_exp = ((2, 1, 2),
                        (2, 2, 2),
                        (2, 3, 2),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)

        ind = (slice(None, None, 2), slice(3), slice(2, 4))
        ind_list_exp = ((1, 1, 2), (1, 1, 3), (1, 1, 4),
                        (1, 2, 2), (1, 2, 3), (1, 2, 4),
                        (1, 3, 2), (1, 3, 3), (1, 3, 4),
                        (3, 1, 2), (3, 1, 3), (3, 1, 4),
                        (3, 2, 2), (3, 2, 3), (3, 2, 4),
                        (3, 3, 2), (3, 3, 3), (3, 3, 4),
                        (5, 1, 2), (5, 1, 3), (5, 1, 4),
                        (5, 2, 2), (5, 2, 3), (5, 2, 4),
                        (5, 3, 2), (5, 3, 3), (5, 3, 4),
                        )
        ind_list_res = tuple(ndarray._multi_ind_iterator(ind, shape))
        for ind_exp, ind_res in zip(ind_list_exp, ind_list_res):
            self.assertTupleEqual(ind_exp, ind_res)




    def test_2D_to_string(self):
        test_array = BaseArray((2, 3), data=(-1, -2.32, 0, 232, 5, -16))
        expected_result = "   -1 -2.32     0 \n  232     5   -16 \n"
        result_string = test_array._to_string()
        self.assertEqual(expected_result, result_string)

    def test_3D_to_string(self):
        testarr = BaseArray((2, 3, 2), data=(15, -0.69, -1.3789, 0, 17, 17.043, 2, 1, 3, 900, 0.0, 7))
        expectedresult = "     15   -0.69 \n-1.3789       0 \n     17  17.043 \n\n      2       1 \n      3     900 \n    0.0       7 \n\n"
        self.assertEqual(expectedresult, testarr._to_string())

    def test_1D_sort(self):
        testarr = BaseArray((5,), data=(5, 3, 8, -3, 9))
        expected_result_1 = BaseArray((5,), data=(-3, 3, 5, 8, 9))
        self.assertListEqual(ndarray.toArray(expected_result_1), ndarray.toArray(testarr._merge_sort()))

    def test_2D_sort(self):
        testarr = BaseArray((3, 3), data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        testarr2 = BaseArray((3, 3), data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        expected_result_vrstice = BaseArray((3, 3), data=(0, 1, 1, -2, 0, 4, 0, 1, 3))
        self.assertTupleEqual(tuple(expected_result_vrstice), tuple(testarr._merge_sort_improved()))
        expected_result_stolpci = BaseArray((3, 3), data=(0, 0, -2, 1, 1, 0, 4, 3, 1))
        self.assertTupleEqual(tuple(expected_result_stolpci), tuple(testarr2._merge_sort_improved("stp")))

    def test_1D_find(self):
        testarr = BaseArray((5, ), data=(0, 2, 4, 0, 5))
        result = testarr._find(3)
        self.assertEqual(result, "V matriki ni tega elementa.")
        result = testarr._find(2)
        self.assertEqual(result, "((1))")
        result = testarr._find(0)
        self.assertEqual(result, "((0)(3))")

    def test_2D_find(self):
        testarr = BaseArray((3, 3), data=(0, 1, 0, 2, 0, 0, 0, 2, 2))
        result = testarr._find(3)
        self.assertEqual(result, "V matriki ni tega elementa.")
        result = testarr._find(1)
        self.assertEqual(result, "((0,1))")
        result = testarr._find(2)
        self.assertEqual(result, "((1,0)(2,1)(2,2))")

    def test_3D_find(self):
        testarr = BaseArray((2, 2, 2), data=(0, 0, 1, 0, 2, 2, 0, 3))
        result = testarr._find(5)
        self.assertEqual(result, "V matriki ni tega elementa.")
        result = testarr._find(3)
        self.assertEqual(result, "((1,1,1))")
        result = testarr._find(2)
        self.assertEqual(result, "((1,0,0)(1,0,1))")

    def test_skalar_mul(self): #Množenje matrike s skalarjem in preverjanje tipa.
        testarr = BaseArray((3, 3), dtype=int, data=(1, 2, 3, 6, 5, 4, 9, 12, 1))
        expectedResult = BaseArray((3, 3), dtype=int, data=(3, 6, 9, 18, 15, 12, 27, 36, 3))
        result = testarr * 3
        self.assertEqual(ndarray.toArray(expectedResult), ndarray.toArray(result))
        self.assertEqual(expectedResult.dtype, result.dtype)
        result = testarr * 3.3
        self.assertEqual(float, result.dtype)

    def test_skalar_div(self):
        testarr = BaseArray((3, 3), dtype=int, data=(3, 6, 9, 18, 15, 12, 27, 36, 3))
        expectedResult = BaseArray((3, 3), dtype=int, data=(1, 2, 3, 6, 5, 4, 9, 12, 1))
        result = testarr / 3
        self.assertEqual(ndarray.toArray(expectedResult), ndarray.toArray(result))
        self.assertEqual(float, result.dtype)
        result = testarr / 3.3
        self.assertEqual(float, result.dtype)

    def test_skalar_log(self):
        testarr = BaseArray((2, 2), dtype=int, data=(1, 2, 4, 8))
        expectedResult = BaseArray((2, 2), dtype=float, data=(0.0, 1.0, 2.0, 3.0))
        result = testarr.__log__(2)
        self.assertEqual(ndarray.toArray(expectedResult), ndarray.toArray(result))
        self.assertEqual(float, result.dtype)

    def test_pow(self):
        testarr = BaseArray((2, 2), dtype=float, data=(1.0, 2.0, 4.0, 8.0))
        expectedResult = BaseArray((2, 2), dtype=float, data=(81.0, 162.0, 324.0, 648.0))
        result = testarr.__pow__(3)
        self.assertEqual(ndarray.toArray(expectedResult), ndarray.toArray(result))
        self.assertEqual(float, result.dtype)

    def test_mul_matrix(self): #Množenje 2D in 3D matrike.
        #Test 2D in int
        testarr = BaseArray((2, 2), dtype=int, data=(1, 3, 4, 2))
        testarr2 = BaseArray((2, 2), dtype=int, data=(4, 2, -1, 5))
        expectedResult = BaseArray((2, 2), dtype=int, data=(1, 17, 14, 18))
        result = testarr*testarr2
        self.assertEqual(ndarray.toArray(expectedResult), ndarray.toArray(result))
        self.assertEqual(expectedResult.dtype, result.dtype)
        #Test 3D return exception
        with self.assertRaises(Exception) as cm:
            testarr = BaseArray((2, 2, 2), dtype=float, data=tuple(range(8)))
            testarr2 = BaseArray((2, 2, 2), dtype=float, data=tuple(range(8)))
            result = testarr * testarr2
        self.assertEqual(('3D multiplication error.',), cm.exception.args)
        #Test 2D invalid shapes
        with self.assertRaises(Exception) as cm:
            testarr = BaseArray((3, 2), dtype=float, data=tuple(range(6)))
            testarr2 = BaseArray((3, 2), dtype=float, data=tuple(range(6)))
            result = testarr * testarr2
        self.assertEqual(('2D multiplication error invalid shapes (3, 2) (3, 2)',), cm.exception.args)

