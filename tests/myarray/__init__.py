from myarray.basearray import BaseArray
import random

nums = tuple(random.randint(1, 50000) for _ in range(122500))
a = BaseArray((350, 350), dtype=int, data=nums)
#b = BaseArray((122500,), dtype=int, data=nums)

a._merge_sort("vrs")
#profile(a._merge_sort("vrs"))
#print(a._to_string())
